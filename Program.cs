﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EHLLAPI;

namespace EHLLAPI
{
	static class Program	
	{
		static void Main (string[] args)
		{
			uint res = EhllapiWrapper.Wait();

			// set params and query session
			res = EhllapiWrapper.SetSessParams("NOATTRB,NOEAB,NOXLATE,NEWRET,TWAIT,SRCHALL,SRCHFRWD,FPAUSE,AUTORESET,QUIET,WRITE_SUPER");
//			res = EhllapiWrapper.SetSessParams("CONPHYS ");
			Console.WriteLine(res);
			EhllapiWrapper.Wait();

			// connect
			res = EhllapiWrapper.Connect("A");
			Console.WriteLine(res);
			EhllapiWrapper.Wait();


			// get position
			res = EhllapiWrapper.GetCursorPos(out int pos);
			Console.WriteLine(res);
			Console.WriteLine(pos);

			// waiting
			Thread.Sleep(10000);


			// write to terminal
			res = EhllapiWrapper.SendStr("TEST");
			Console.WriteLine(res);
			EhllapiWrapper.Wait();

			// disabling input
			res = EhllapiWrapper.DisableOperatorInput();
			Console.WriteLine(res);
			EhllapiWrapper.Wait();


			// waiting
			Thread.Sleep(10000);

			// enabling input
			res = EhllapiWrapper.EnableOperatorInput();
			Console.WriteLine(res);
			EhllapiWrapper.Wait();


			// set position
			res = EhllapiWrapper.SetCursorPos(pos);
			Console.WriteLine(res);
			EhllapiWrapper.Wait();

			// write to terminal
			res = EhllapiWrapper.SendStr("1");
			Console.WriteLine(res);
			EhllapiWrapper.Wait();


			// disconnect
			res = EhllapiWrapper.Disconnect("A");
			Console.WriteLine(res);



		}
	}
	
}
