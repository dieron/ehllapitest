﻿using System;
using System.Runtime.InteropServices;

namespace EHLLAPI
{
	public class User32Api
	{


		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr SetFocus(IntPtr hWnd);


		[DllImport("user32.dll")]
		public static extern IntPtr SetCursor(IntPtr handle);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool SetForegroundWindow(IntPtr hWnd);
	}
}